# CRUD with FAST API and Docker

# Change to the folder poject
```
cd crud-api-fast-api
```

# Create the database
```SQL
CREATE DATABASE `test` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
```

# Create the docker network
```
docker network create nt_bkpy
```

# Build image
```
docker build --tag=fast-api-crud:1.0 ./docker/
```
# Run docker compose
```
docker-compose ./docker/docker-compose.yml up -d
```