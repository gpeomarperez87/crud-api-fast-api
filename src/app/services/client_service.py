from typing import List

from data_access.repositories.client_repository import ClientRepository
from models.client_model import ClientModel
from schemas.client_schema import ClientBaseSchema, ClientSchemaDB


class ClientService:

    def __init__(
        self,
        client_repository: ClientRepository
    ):
        self.client_repository = client_repository

    def get_all(self):
        return self.client_repository.get_all()

    def get_one(self, id: int) -> List[ClientSchemaDB]:
        return self.client_repository.get_one(id)

    def insert(
            self, data_insert: ClientBaseSchema
    ) -> ClientSchemaDB:
        """
        Aqui va la logica para insertar un registro
        """
        return self.client_repository.insert(data_insert)

    def update(
            self,
            id: int,
            data_update: ClientBaseSchema
    ) -> ClientSchemaDB:
        return self.client_repository.update(id, data_update)

    def delete(self, id: int):
        return self.client_repository.delete(id)
