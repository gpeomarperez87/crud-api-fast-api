from fastapi import FastAPI
from starlette.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware

from containers.main_container import Container
from routers import client_router

main_container = Container()

main_container.wire(modules=[client_router])

db = main_container.db()
db.create_database()

app = FastAPI(
    title="CRUD with Fast API",
    description="This is a CRUD project with Fast API and " +
    "dependency injector",
    version="0.0.1",
    openapi_url="/api/v1/openapi.json"
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.container = main_container

app.include_router(client_router.clientRouter)


@app.get('/')
async def root():
    return RedirectResponse(url="/docs/")
