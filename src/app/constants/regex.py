class RegexConstant():
    general_text = "(^[a-zA-ZáéíóúÁÉÍÓÚ]{1})([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\.])+"

    rfc = "^[A-ZÑ&]{3,4}\d{6}(?:[A-Z\d]{3})?$"

    name = "(^[a-zA-ZáéíóúÁÉÍÓÚ]{1})([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\.])+"

    mother_last_name = "^(([a-zA-ZáéíóúÁÉÍÓÚ]{1})" +\
        "([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\.])+)*$"

    bussines_name = "^(([a-zA-ZáéíóúÁÉÍÓÚ]{1})" +\
        "([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\-\.])+)*$"

    street_name = "^([a-zA-ZáéíóúÁÉÍÓÚ]{1})" +\
        "([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\-\.])+$"

    street_name1 = "^(([a-zA-ZáéíóúÁÉÍÓÚ]{1})" +\
        "([a-zA-ZñáéíóúÑÁÉÍÓÚ\s\-\.])+)*$"

    outdoor_number = "^([a-zA-Z\d](|[\.\s\-]*))+$"

    interior_number = "^([a-zA-Z\d](|[\.\s\-]*))*$"

    email = "^([\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4})*$"

    mobile_phone_number = "^[0-9]+$"

    phone_number = "^[0-9]*$"

    sex = "^M|H|N$"

    time = "^[0-2]{1}[0-9]{1}:[0-9]{2}(|:[0-9]{2})$"
