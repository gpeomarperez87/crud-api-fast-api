from data_access.repositories.base_repository import BaseRepository
from schemas.client_schema import ClientBaseSchema
from data_access.repositories.client_repository import ClientRepository
from models.client_model import ClientModel
from services.client_service import ClientService
from data_access.connection import Connection
from dependency_injector import containers, providers


class Container(containers.DeclarativeContainer):

    # config = providers.Configuration()

    db = providers.Singleton(Connection)

    client_repository = providers.Factory(
        BaseRepository[ClientModel, ClientBaseSchema],
        model=ClientModel,
        session=db.provided.session,
    )

    client_service = providers.Factory(
        ClientService,
        client_repository=client_repository,
    )
