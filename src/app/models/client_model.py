from data_access.connection import MembershipBase
from sqlalchemy import Column
from sqlalchemy.dialects.mysql import (
    BIGINT, CHAR, DATE, INTEGER, LONGTEXT, VARCHAR)


class ClientModel(MembershipBase):
    __tablename__ = "clients"

    id = Column(
        BIGINT, autoincrement=True,
        nullable=False,
        primary_key=True, index=True
    )

    business_name = Column(LONGTEXT(), nullable=False, default='')
    rfc = Column(VARCHAR(15), nullable=False, default='XAXX010101000')
    name = Column(VARCHAR(100), nullable=False, default='')
    last_name = Column(VARCHAR(100), nullable=False, default='')
    mother_last_name = Column(VARCHAR(100), nullable=False, default='')
    street_name = Column(VARCHAR(100), nullable=False, default='')
    outdoor_number = Column(VARCHAR(50), nullable=False, default='')
    interior_number = Column(VARCHAR(50), nullable=False, default='')
    suburb_id = Column(
        INTEGER(),
        nullable=False, default=0
    )
    postal_code = Column(
        INTEGER(),
        nullable=False, default=0
    )
    township_id = Column(
        INTEGER(),
        nullable=False, default=0
    )
    street_name1 = Column(VARCHAR(100), nullable=False, default='')
    street_name2 = Column(VARCHAR(100), nullable=False, default='')
    e_mail = Column(
        LONGTEXT(), nullable=False,
        default='mockmail@mockmail.com'
    )
    mobile_phone_number = Column(
        VARCHAR(10),
        nullable=False, default='')
    phone_number = Column(VARCHAR(10), nullable=False, default='')
    sex = Column(CHAR(1), nullable=False, default='')
    birthday_date = Column(DATE(), nullable=False, default='1900-01-01')
