from models.client_model import ClientModel
from schemas.client_schema import ClientBaseSchema

from .base_repository import BaseRepository


class ClientRepository(BaseRepository[ClientModel, ClientBaseSchema]):
    pass
