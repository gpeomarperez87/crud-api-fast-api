from typing import Any, Callable, Generic, List, Optional, Type, TypeVar
from data_access.connection import MembershipBase
from contextlib import AbstractContextManager
from pydantic import BaseModel
from sqlalchemy.orm import Session

ModelType = TypeVar("ModelType", bound=MembershipBase)
SchemaType = TypeVar("SchemaType", bound=BaseModel)


class BaseRepository(Generic[ModelType, SchemaType]):

    def __init__(
            self, model: Type[ModelType],
            session: Callable[..., AbstractContextManager[Session]]):
        self.model = model
        self.session = session

    def get_all(
        self, skip: int = 0, limit: int = 100
    ) -> List[ModelType]:
        with self.session() as session:
            return session.query(self.model)\
                .offset(skip).limit(limit).all()

    def get_one(
        self, id: int = 0
    ) -> Optional[ModelType]:
        with self.session() as session:
            return session.query(self.model).filter_by(id=id).first()

    def insert(
        self, data_insert: SchemaType
    ) -> ModelType:
        with self.session() as session:
            data_insert = self.model(**data_insert.dict())
            session.add(data_insert)
            session.commit()
            session.refresh(data_insert)
            return data_insert

    def update(
        self, id: int, data_update: SchemaType
    ) -> SchemaType:
        with self.session() as session:
            session.query(self.model).filter_by(id=id).update(
                data_update.dict()
            )
            session.commit()
            return data_update

    def delete(self, id: int) -> Any:
        with self.session() as session:
            data = session.query(self.model)\
                .filter_by(id=id)\
                .first()
            session.delete(data)
            session.commit()
            return {"message": "User deleted"}
