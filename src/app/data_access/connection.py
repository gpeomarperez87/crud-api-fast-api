from contextlib import contextmanager, AbstractContextManager
from typing import Callable
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

MembershipBase = declarative_base()


class Connection:
    _membership_connection_string = "mysql+mysqlconnector://" +\
        "root:mypassword@server-database/test"

    def __init__(self) -> None:
        self._membership_engine = create_engine(
            self._membership_connection_string
        )

        self._emergency_session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=False
            ))

        self._emergency_session.configure(binds={
            MembershipBase: self._membership_engine
        })
    
    def create_database(self) -> None:
        MembershipBase.metadata.create_all(self._membership_engine)

    @contextmanager
    def session(self) -> Callable[..., AbstractContextManager[Session]]:
        session: Session = self._emergency_session
        try:
            yield session
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()
