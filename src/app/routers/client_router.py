from typing import List

from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, Path
from schemas.client_schema import ClientBaseSchema, ClientSchemaDB
from containers.main_container import Container
from services.client_service import ClientService

clientRouter = APIRouter()


@clientRouter.get(
    '/clients',
    response_model=List[ClientSchemaDB],
    summary="Get the data of all clients",
    tags=['clients']
)
@inject
async def get_clients(
    client_service: ClientService = Depends(
        Provide[Container.client_service]
    )
):
    return client_service.get_all()


@clientRouter.get(
    '/clients/{client_id}',
    response_model=ClientSchemaDB,
    summary="Get the data of a specific client",
    tags=['clients']
)
@inject
async def get_client(
    *,
    client_id: int = Path(..., example=1),
    client_service: ClientService = Depends(
        Provide[Container.client_service]
    )
):
    return client_service.get_one(client_id)


@clientRouter.post(
    '/clients',
    summary="Save client data",
    tags=['clients']
)
@inject
async def save_client(
    data_insert: ClientBaseSchema,
    client_service: ClientService = Depends(
        Provide[Container.client_service]
    )
):
    return client_service.insert(data_insert)


@clientRouter.patch(
    '/clients/{client_id}',
    response_model=ClientBaseSchema,
    summary="Update partial client data",
    tags=['clients']
)
@inject
async def patch_client(
    *,
    client_id: int = Path(..., example=1),
    data_update: ClientBaseSchema,
    client_service: ClientService = Depends(
        Provide[Container.client_service]
    )
):
    return client_service.update(client_id, data_update)


@clientRouter.delete(
    '/clients/{client_id}',
    summary="Delete the data of a specific client",
    tags=['clients']
)
@inject
async def delete_client(
    *,
    client_id: int = Path(..., example=1),
    client_service: ClientService = Depends(
        Provide[Container.client_service]
    )
):
    return client_service.delete(client_id)
