from pydantic import BaseModel, Field
from typing import Optional
from datetime import date
from schemas.base_schema import BaseSchema
from constants.regex import RegexConstant


class ClientBaseSchema(BaseModel):
    business_name: Optional[str] = Field(
        None,
        title="Business name",
        description="The business name from client",
        regex=RegexConstant.bussines_name,
        max_length=300,
        example="Fake business name",

    )
    rfc: str = Field(
        title="RFC",
        description="The federal taxpayer registry",
        regex=RegexConstant.rfc,
        max_length=13,
        example="XAXX010101000"
    )
    name: str = Field(
        title="Name",
        description="The client name",
        regex=RegexConstant.name,
        max_length=100,
        example="John"
    )
    last_name: str = Field(
        title="Last name",
        description="The client last name",
        regex=RegexConstant.name,
        max_length=100,
        example="Doe"
    )
    mother_last_name: Optional[str] = Field(
        None,
        title="Mother's last name",
        description="The client mother's last name",
        regex=RegexConstant.mother_last_name,
        max_length=100,
        example="Test"
    )
    street_name: str = Field(
        title="Street name",
        description="The client's address street name",
        regex=RegexConstant.street_name,
        max_length=100,
        example="Street name"
    )
    outdoor_number: str = Field(
        title="Outdoor number",
        description="The client's address outdoor number",
        regex=RegexConstant.outdoor_number,
        max_length=50,
        example="222"
    )
    interior_number: Optional[str] = Field(
        None,
        title="Interior number",
        description="The client's address interior number",
        regex=RegexConstant.interior_number,
        max_length=50,
        example="A dep 27"
    )
    suburb_id: int = Field(
        title="Suburb id",
        description="The client's address suburb id from suburbs table",
        gt=0,
        example=23
    )
    postal_code: int = Field(
        title="Postal code",
        description="The client's address postal code",
        gt=0,
        example=12345
    )
    township_id: int = Field(
        title="Township id",
        description="The client's address township id from " +
        "townships table",
        gt=0,
        example=23
    )
    street_name1: Optional[str] = Field(
        None,
        title="First street name",
        description="The client's address first street name " +
        "for easy location",
        regex=RegexConstant.street_name1,
        max_length=50,
        example="Street name location"
    )
    street_name2: Optional[str] = Field(
        None,
        title="Second street name",
        description="The client's address second street name " +
        "for easy location",
        regex=RegexConstant.street_name1,
        max_length=50,
        example="Other street name location"
    )
    e_mail: Optional[str] = Field(
        None,
        title="Email",
        description="The client's email",
        regex=RegexConstant.email,
        example="fakemail@domainsample.com"
    )
    mobile_phone_number: str = Field(
        title="Mobile phone number",
        description="The client's mobile phone number",
        regex=RegexConstant.mobile_phone_number,
        max_length=10,
        example="5544332211"
    )
    phone_number: Optional[str] = Field(
        None,
        title="Phone number",
        description="The client's phone number",
        regex=RegexConstant.phone_number,
        max_length=10,
        example="5544332211"
    )
    sex: Optional[str] = Field(
        None,
        title="Sex",
        description="The client's sex to indicate whether it is a " +
        "woman(M), man(H) or non-binary person(N)",
        regex=RegexConstant.sex,
        max_length=1,
        example="M"
    )
    birthday_date: Optional[date] = Field(
        None,
        title="Birthday Date",
        description="The client's birthday date",
        example="1987-03-04"
    )


class ClientSchemaDB(ClientBaseSchema, BaseSchema):
    class Config:
        orm_mode = True
