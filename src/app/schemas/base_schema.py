from pydantic import BaseModel, Field


class BaseSchema(BaseModel):
    id: int = Field(
        title="Id",
        description="This is the primary key",
        example="1"
    )
